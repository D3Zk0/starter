<?php

namespace d3x\starter\Traits;

trait ModelHelpers
{
    public static function toFillable($data)
    {
        return array_intersect_key($data, array_flip((new self)->getFillable()));
    }

    public static function create($attributes = [])
    {
        return static::query()->create(self::toFillable($attributes));
    }

    public function update($attributes = [], $options = [])
    {
        return parent::update(self::toFillable($attributes), $options);
    }

    public static function store($data)
    {
        if (isset($data["id"]) && $model = self::find($data["id"])) {
            $model->update($data);
            return $model;
        }
        return self::create($data);
    }
}
