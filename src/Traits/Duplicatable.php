<?php

namespace d3x\starter\Traits;

use Illuminate\Database\Eloquent\Model;

trait Duplicatable
{

    public function duplicate()
    {
        // Ustvari kopijo trenutne instance modela
        $newModel = $this->replicate();
        $newModel->push();

        // Preveri, ali ima model relacije
        foreach (self::DUPLICATABLE_RELATIONS as $relationName) {
            foreach ($this->$relationName as $relatedModel) {
                $newRelatedModel = $relatedModel->replicate();
                $newRelatedModel->setAttribute($this->getForeignKey(), $newModel->getKey());
                $newRelatedModel->save();
            }
        }

        return $newModel;
    }


}