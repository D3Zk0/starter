<?php

namespace d3x\starter\Traits;

use App\Mail\ResetPassword;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

trait CanResetPassword
{
    public function sendResetPasswordEmail()
    {
        try {
            DB::beginTransaction();
            $token = Str::random(64);
            DB::table('password_resets')->insert([
                'email' => $this->email,
                'token' => $token,
                'created_at' => Carbon::now()
            ]);
            Mail::to([$this->email])->send(new ResetPassword($token, $this));
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw new \Exception($e->getMessage());
        }
    }
}
