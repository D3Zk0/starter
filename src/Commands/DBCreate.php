<?php

namespace d3x\starter\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


class DBCreate extends Command
{
    protected $signature = 'db:create {name?}';
    protected $description = 'Create a new MySQL database based on the database config file or the provided name';

    public function handle()
    {
        try {
            $schemaName = $this->argument('name') ?: env("DB_DATABASE");
            $charset = config("database.connections.mysql.charset",'utf8mb4');
            $collation = config("database.connections.mysql.collation",'utf8mb4_unicode_ci');

            Config::set("database.connections.mysql.database", null);
            $query = "CREATE DATABASE IF NOT EXISTS $schemaName CHARACTER SET $charset COLLATE $collation;";
            DB::connection("mysql")->statement($query);
            Config::set("database.connections.mysql.database", $schemaName);

            $this->info("DATABASE {$schemaName} successfully created!");
        } catch (Exception $e) {
            $this->info("There was an error creating the database {$e->getMessage()}");
        }
        return 0;
    }
}
