<?php

namespace d3x\starter\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;


class AllClear extends Command
{
    protected $signature = 'all:clear';
    protected $description = 'Single command that clears all caches!';

    public function handle()
    {
        try {
            Artisan::call("cache:clear");
            Artisan::call("route:clear");
            Artisan::call("view:clear");
            Artisan::call("config:clear");

            $this->info("Caches successfully cleared");
        } catch (Exception $e) {
            $this->info("There was an error deleting tokens!");
        }
        return 0;
    }
}
