<?php

namespace d3x\starter\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;


class DBReset extends Command
{
    protected $signature = 'db:reset';
    protected $description = 'Wipes database & remigrates it (+ seed)';

    public function handle()
    {
        try {
            Artisan::call("db:wipe");
            Artisan::call("migrate --seed");

            $this->info("DB successfully reset");
        } catch (Exception $e) {
            $this->info("There was an error reseting DB: ".$e->getMessage());
        }
        return 0;
    }
}
