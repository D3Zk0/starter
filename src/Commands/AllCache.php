<?php

namespace d3x\starter\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;


class AllCache extends Command
{
    protected $signature = 'all:cache';
    protected $description = 'Single command that clears all caches!';

    public function handle()
    {
        try {
            Artisan::call("view:cache");
            Artisan::call("config:cache");
            Artisan::call("event:cache");
            Artisan::call("route:cache");

            $this->info("Application successfully cached!");
        } catch (Exception $e) {
            $this->info("There was an error deleting tokens!");
        }
        return 0;
    }
}
