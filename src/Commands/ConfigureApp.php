<?php

namespace d3x\starter\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;


class ConfigureApp extends Command
{
    protected $signature = 'configure:app';
    protected $description = 'Command configures app.';

    public function handle()
    {
        try {
            shell_exec("cp .env.example .env");
            Artisan::call("key:generate");
            Artisan::call("storage:link");
            Artisan::call("optimize");
            Artisan::call("cache:clear");
            Artisan::call("config:clear");
            Artisan::call("db:reset");

            $this->info("App successfully configured!");
        } catch (Exception $e) {
            $this->info("There was an error deleting tokens!");
        }
        return 0;
    }
}
