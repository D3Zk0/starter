<?php

namespace d3x\starter;

use d3x\starter\Commands\AllCache;
use d3x\starter\Commands\AllClear;
use d3x\starter\Commands\ConfigureApp;
use d3x\starter\Commands\DBCreate;
use d3x\starter\Commands\DBReset;
use d3x\starter\Providers\ResponseServiceProvider;
use Illuminate\Support\ServiceProvider;
use d3x\starter\Providers\QueryServiceProvider;


class StarterServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                AllCache::class,
                AllClear::class,
                ConfigureApp::class,
                DBReset::class,
                DBCreate::class,
            ]);
        }
    }

    public function register()
    {
        $this->app->register(QueryServiceProvider::class);
        $this->app->register(ResponseServiceProvider::class);
    }
}
