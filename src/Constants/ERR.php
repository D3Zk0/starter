<?php

namespace d3x\starter\Constants;

class ERR
{
    public const MESSAGE = "Prišlo je do napake!";
    public const NOTFOUND = 404;
    public const UNPROCESSABLE = 422;
    public const UNAUTHORIZED = 401;
    public const FORBIDDEN = 403;
    public const BAD = 400;
    public const SERVER = 500;
}
