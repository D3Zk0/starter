<?php

namespace d3x\starter\Constants;

class SUCC
{
    public const QUERY_MSG = "Poizvedba uspešna!";
    public const UPDATE_MSG = "Posodobitev uspešna!";
    public const CREATE_MSG = "Kreacija uspešna!";
    public const DELETE_MSG = "Brisanje uspešno!";
    public const OK = 200;
}
