<?php

namespace d3x\starter\Providers;

use d3x\Starter\Constants\ERR;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ResponseServiceProvider extends ServiceProvider
{

    public function boot()
    {
        Response::macro("success", function ($message = "Uspešno izvedeno", $data = null) {
            return response()->json(compact("message", "data"));
        });

        Response::macro("error", function ($status, $msg = null, $exception = null, $errors = []) {
            $loggable = "$status { MSG: $msg, ";
            if ($exception)
                $loggable .= "\n FILE: {$exception->getFile()} \n LINE: {$exception->getLine()}  \n MESSAGE: {$exception->getMessage()} }";
            if ($status !== ERR::UNPROCESSABLE)
                Log::error($loggable);
            if ($status === ERR::SERVER) {
                //TODO-DEX: DODAJ MAIL O NAPAKI
            }

            $exception = (array)$exception;
            $message = $msg;
            return response()->json(compact("msg", "message", "exception", "errors"), $status);
        });
    }

    public function register()
    {

    }
}
