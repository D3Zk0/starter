<?php

namespace d3x\starter\Providers;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\ServiceProvider;

class QueryServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Builder::macro('search', function ($cols = [], $search = null, $strict = false) {
            $cols = $cols ?: $this->DEFAULT_SEARCH_COLS;
            if (!$search) return $this;

            // Split the search term into individual words
            $searchTerms = explode(' ', $search);

            $this->where(function ($query) use ($cols, $searchTerms, $strict) {
                foreach ($searchTerms as $term) {
                    $query->where(function ($termQuery) use ($cols, $term, $strict) {
                        $flag = true;
                        foreach ($cols as $col) {
                            $term = strtolower($term); // Convert each term to lowercase
                            if ($strict) {
                                if ($flag) {
                                    $termQuery->where($col, $term);
                                    $flag = false;
                                } else {
                                    $termQuery->orWhere($col, $term);
                                }
                            } else {
                                if ($flag) {
                                    $termQuery->whereRaw("
        lower(
            regexp_replace(
                CASE 
                    WHEN JSON_VALID($col) THEN JSON_UNQUOTE($col) 
                    ELSE $col 
                END, 
                '[\\\\[\\\\] ]', ''
            )
        ) like ?", ["%{$term}%"]);
                                    $flag = false;
                                } else {
                                    $termQuery->orWhereRaw("
        lower(
            regexp_replace(
                CASE 
                    WHEN JSON_VALID($col) THEN JSON_UNQUOTE($col) 
                    ELSE $col 
                END, 
                '[\\\\[\\\\] ]', ''
            )
        ) like ?", ["%{$term}%"]);
                                }
                            }
                        }
                    });
                }
            });
            return $this;
        });

        Builder::macro("order", function ($orderStates = []) {
            if (!$orderStates) return $this;
            foreach ($orderStates as $state) {
                $dir = $state->dir ?: "acs";
                $this->orderBy($state->col, $dir);
            }
            return $this;
        });

        Builder::macro("whereOverlaps", function ($start, $end, $from, $to, $strict = true) {
            if (!$start || !$end || !$from || !$to)
                return $this;
            return $this->where(function ($q) use ($start, $end, $from, $to) {
                $q->where(function ($q2) use ($to, $start, $end, $from) {
                    $q2
                        ->where($start, '>=', $from)
                        ->where($start, '<=', $to);
                })->orWhere(function ($q2) use ($from, $start, $end, $to) {
                    $q2
                        ->where($end, '>=', $from)
                        ->where($end, '<=', $to);
                })->orWhere(function ($q2) use ($start, $end, $from, $to) {
                    $q2->where($start, "<=", $from)->where($end, ">=", $to);
                });
            });
        });


    }

    public function register()
    {

    }
}
