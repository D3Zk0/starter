<?php

namespace d3x\starter\Functions;


class General
{
    public static function niceDate($date){
        return (new \DateTime($date))->format('d.m.Y');
    }

    public static function niceDateTime($date){
        return (new \DateTime($date))->format('d.m.Y H:i');
    }
    public static function comma_format($number, $tousands_separator = ' ')
    {
        return number_format($number, 2, ',', $tousands_separator);
    }

    public static function euroFormat($number, $tousands_separator = ' ')
    {
        return number_format($number, 2, ',', $tousands_separator) . ' €';
    }

}