<?php

namespace d3x\starter\Functions;

class WPM
{
    public static function arr_only(array $arr, array $keys): array
    {
        return array_intersect_key($arr, array_flip($keys));
    }

    public static function ob_only($ob, array $keys): array
    {
        return array_intersect_key(get_object_vars($ob), array_flip($keys));
    }
}