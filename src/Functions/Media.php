<?php

namespace d3x\starter\Functions;

use Illuminate\Support\Facades\Storage;

class Media
{
    public static function save($content, $folder, $name = null)
    {
        $folderPath = 'public/' . $folder;
        $path = $folderPath . "/" . $name;
        if (!Storage::exists($folderPath)) {
            Storage::makeDirectory($folderPath); //creates directory
        }
        Storage::put($path, $content);
        return str_replace("public", "/storage", $path);
    }

    public static function delete($path)
    {
        // Replace '/storage' with 'public' to get the correct path for Storage facade
        $storagePath = str_replace("/storage", "public", $path);

        if (Storage::exists($storagePath)) {
            Storage::delete($storagePath);
            return true; // File deleted successfully
        }

        return false; // File not found
    }


}
