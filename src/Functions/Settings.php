<?php

namespace d3x\starter\Functions;

class Settings
{
    public static function setConfig($config, $settings){
        foreach ($config as $key => $value)
            $settings->{$key} = $value;
        $settings->save();
    }
}
